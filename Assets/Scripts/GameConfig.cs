using UnityEngine;

namespace AlexSetoTest
{
    [CreateAssetMenu(fileName = "Game Config")]
    public class GameConfig : ScriptableObject
    {
        
    #region Fields

        [Header("Crates")]
        [Range(0,10000)] public int minXAreaSpawn = 0;
        [Range(0,10000)] public int maxXAreaSpawn = 500;
        [Range(0,10000)] public int minZAreaSpawn = 0;
        [Range(0,10000)] public int maxZAreaSpawn = 500;
        public float spawnTime = 10f;        
        public int maxAmountItems = 5;

        [Header("Items")]
        public ItemConfig[] itemConfigs;
        
        [Header("Astronauts")]        
        public AstronautConfig astronautConfig;

    #endregion    
    }

#region Structs

    [System.Serializable]
    public struct AstronautConfig
    {
        public float visionRange;
        public Vitals[] vitals;
    }

    [System.Serializable]
    public struct ItemConfig
    {
        public VitalType vitalType;
        public Sprite m_vitalIcon;
        [Range(1,50)] public int vitalAmount;
    }
#endregion
}
