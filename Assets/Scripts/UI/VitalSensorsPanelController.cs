using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace AlexSetoTest
{
    public class VitalSensorsPanelController : MonoBehaviour
    {
#region Public Methods
        public void InitNewCharacterAmount(PlayerController astronaut)
        {            
            m_charactersList.Add(astronaut);
            InitVitalBars();

            astronaut.ItemUsed += RefreshVitalBarByType;
            astronaut.StatDecreased += RefreshVitalBarByType;
        }  
#endregion

#region Private Methods

        private void InitVitalBars()
        {            
            foreach (VitalType vitalType in Enum.GetValues(typeof(VitalType)))
            {
                RefreshVitalBarByType(vitalType);
            }                        
        }

        private void RefreshVitalBarByType(VitalType vitalType)
        {
            switch(vitalType)
            {
                case VitalType.Food:
                    m_totalFood = 0;                        
                    break;
                case VitalType.Water:
                    m_totalWater = 0;
                    break;
                case VitalType.Oxygen:
                    m_totalOxygen = 0;
                    break;
            }

            foreach (PlayerController character in m_charactersList)
            {
                GetAverageAmountByType(character.CharacterStats, vitalType);                
            }

            SetVitalBarByType(vitalType);
        }

        private void GetAverageAmountByType(PlayerStats charStats, VitalType vitalType)
        {
            switch(vitalType)
            {
                case VitalType.Food:
                    m_totalFood += charStats.GetCurrentStatAmountNormalized(vitalType); 
                    m_averageFood = m_totalFood / m_charactersList.Count;                     
                    break;
                case VitalType.Water:
                    m_totalWater += charStats.GetCurrentStatAmountNormalized(vitalType);
                    m_averageWater = m_totalWater / m_charactersList.Count;
                    break;
                case VitalType.Oxygen:
                    m_totalOxygen += charStats.GetCurrentStatAmountNormalized(vitalType);
                    m_averageOxygen = m_totalOxygen / m_charactersList.Count;
                    break;
            }
        }       

        private void SetVitalBarByType(VitalType vitalType)
        {
            switch(vitalType)
            {
                case VitalType.Food:
                    m_foodBar.fillAmount = m_averageFood;
                    break;
                case VitalType.Water:
                    m_waterBar.fillAmount = m_averageWater;
                    break;
                case VitalType.Oxygen:
                    m_oxygenBar.fillAmount = m_averageOxygen;
                    break;
            }
        }        
#endregion

#region Fields

        [SerializeField] private Image m_foodBar = null;
        [SerializeField] private Image m_waterBar = null;
        [SerializeField] private Image m_oxygenBar = null;
        private List<PlayerController> m_charactersList = new List<PlayerController>();
        private float m_totalFood = 0f;
        private float m_totalWater = 0f;
        private float m_totalOxygen = 0f;
        private float m_averageFood = 0f;
        private float m_averageWater = 0f;
        private float m_averageOxygen = 0f;
#endregion
    }
}
