using UnityEngine;
using TMPro;

namespace AlexSetoTest
{
    public class UIManager : MonoBehaviour
    {
#region Public Methods
        public void OpenAstronautPanel(PlayerController astronaut)
        {
            m_astronautPanel.Initialize(astronaut);
            m_spawnButton.SetActive(false);            
        }

        public void CloseAstronautPanel()
        {
            m_astronautPanel.ClosePanel();
            m_spawnButton.SetActive(true);   
        }

        public bool IsAstronautPanelOpen()
        {
            return m_astronautPanel.gameObject.activeSelf;
        }

        public void OpenGameEndPanel(PlayerController astronaut)
        {           
            m_gameEndPanel.SetActive(true);
            m_astronautPanel.gameObject.SetActive(false);
            m_spawnButton.SetActive(false);

            m_astronaudDiedText.text = string.Format("{0}  died", astronaut.AstronautName);
        }
#endregion

#region Fields

        [SerializeField] private AstronautPanelController m_astronautPanel = null;
        [SerializeField] private GameObject m_gameEndPanel = null;
        [SerializeField] private TextMeshProUGUI m_astronaudDiedText = null;
        [SerializeField] private GameObject m_spawnButton = null;
#endregion
    }
}
