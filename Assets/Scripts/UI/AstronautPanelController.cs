using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace AlexSetoTest
{
    public class AstronautPanelController : MonoBehaviour
    {
        
#region Public Methods
        public void Initialize(PlayerController astronaut)
        {            
            gameObject.SetActive(true);
            m_character = astronaut;
            m_astronauCamRenderer.SetTargetToFollow(m_character.transform);
            m_astronautNameText.text = astronaut.AstronautName;

            foreach (Vitals vital in m_character.CharacterStats.CharVitals)
            {
                SetVitalBar(vital.vitalType);
            } 

            SetItemsInventory();
            astronaut.StatDecreased += SetVitalBar;
            astronaut.ItemUsed += SetVitalBar;
            astronaut.OnOpenedCrate += SetItemsInventory;
        } 

        public void ClosePanel()
        {
            m_character.StatDecreased -= SetVitalBar;
            m_character.ItemUsed -= SetVitalBar;
            m_character.OnOpenedCrate -= SetItemsInventory;

            gameObject.SetActive(false);

            //TODO: Dont destroy, use a pool
            foreach (Transform item in m_inventory)
            {
                Destroy(item.gameObject);
            }
        }

        public void OnEditNameClicked()
        {                        
            m_astronautNameText.enabled = true;
            m_astronautNameText.Select();
        }

        public void OnEditNameFinished()
        {
            m_astronautNameText.enabled = false;
            m_character.SetAstronautName(m_astronautNameText.text);
        }
#endregion

#region Private Methods

        private void SetVitalBar(VitalType vitalType)
        {
            switch(vitalType)
            {
                case VitalType.Food:
                    m_foodBar.fillAmount = m_character.CharacterStats.GetCurrentStatAmountNormalized(vitalType);
                    break;
                case VitalType.Water:
                    m_waterBar.fillAmount = m_character.CharacterStats.GetCurrentStatAmountNormalized(vitalType);
                    break;
                case VitalType.Oxygen:
                    m_oxygenBar.fillAmount = m_character.CharacterStats.GetCurrentStatAmountNormalized(vitalType);
                    break;
            }
        }

        private void SetItemsInventory()
        {            
            //TODO: Create an item pool            
            foreach (ItemModel itemModel in m_character.CharacterInventory.ItemsList)
            {
                ItemController newItem = Instantiate(m_itemPrefab, m_inventory);
                newItem.Initialize(itemModel, m_character.OnItemUsed);                
            }
        }
#endregion

#region Fields

        [SerializeField] private TMP_InputField m_astronautNameText = null;
        [SerializeField] private Image m_foodBar = null;
        [SerializeField] private Image m_waterBar = null;
        [SerializeField] private Image m_oxygenBar = null;
        [SerializeField] private Transform m_inventory = null;
        [SerializeField] private ItemController m_itemPrefab = null;
        [SerializeField] private CameraFollowTarget m_astronauCamRenderer = null;
        private PlayerController m_character = null;
#endregion
    }
}