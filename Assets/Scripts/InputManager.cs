using UnityEngine;

namespace AlexSetoTest
{
    public class InputManager : MonoBehaviour
    {
#region Public Methods
        public void CheckCameraScreenEdges()
        {
            if (Input.mousePosition.x > Screen.width - m_edgeOffset && m_camera.transform.position.x < k_terrainLimitBoundary - Screen.width/2)
            {
                m_camera.transform.position += new Vector3 (Time.deltaTime * m_speed, 0f, 0f);
            }
                
            if (Input.mousePosition.x < m_edgeOffset && m_camera.transform.position.x > Screen.width/2)
            {
                m_camera.transform.position -= new Vector3 (Time.deltaTime * m_speed, 0f, 0f);
            }
                
            if (Input.mousePosition.y > Screen.height - m_edgeOffset && m_camera.transform.position.z < k_terrainLimitBoundary - Screen.height/2)
            {
                m_camera.transform.position += new Vector3 (0f, 0f, Time.deltaTime * m_speed);		
            }
        
            if (Input.mousePosition.y < m_edgeOffset && m_camera.transform.position.z > 0)
            {
                m_camera.transform.position -= new Vector3 (0f, 0, Time.deltaTime * m_speed);		
            }
        }

        public void CheckUserInput()
        {
            if(m_uiManager.IsAstronautPanelOpen())
            {
                return;
            }

            if(Input.GetMouseButtonDown(0))
            {
                m_clickTime = Time.time;  
                RaycastHit hit;
                if (Physics.Raycast(m_camera.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    if(hit.collider.CompareTag("Player"))
                    {
                        m_astronautSelected = hit.collider.GetComponentInParent<PlayerController>();                        
                        m_zCamPos = m_camera.WorldToScreenPoint(m_astronautSelected.transform.position).z;
                    }
                }              
            }

            if(Input.GetMouseButton(0))
            {
                if(Time.time - m_clickTime >= k_clickToDraggTrheshold)
                {
                    if(m_astronautSelected)
                    {
                        m_astronautSelected.OnBeginDrag();
                        m_isDragging = true;
                        m_zCamPos = m_camera.WorldToScreenPoint(m_astronautSelected.transform.position).z;

                        Vector3 screenPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, m_zCamPos);
                        Vector3 worldPosition = m_camera.ScreenToWorldPoint(screenPosition);
                        
                        m_astronautSelected.transform.position = worldPosition;
                    }     
                }           
            }
        
            if(Input.GetMouseButtonUp(0) && m_astronautSelected)
            {
                if(Time.time - m_clickTime < k_clickToDraggTrheshold)
                {
                    m_uiManager.OpenAstronautPanel(m_astronautSelected);
                }                

                if(m_isDragging)
                {
                    m_astronautSelected.OnEndDrag();
                    m_isDragging = false;                    
                }

                m_astronautSelected = null;
            }
        }    

        public Vector3 GetCenterPositionCamera()
        {
            Ray ray = m_camera.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2 ));
            RaycastHit hitPoint;
 
            if(Physics.Raycast(ray, out hitPoint))
            {
                if(hitPoint.collider.CompareTag("Terrain"))
                {
                    return hitPoint.point;
                }                
            }
            
            return Vector3.zero;
        }

        public Vector3 GetYTerrainPositionAt(Vector3 cratePos)
        {
            Ray ray = new Ray(cratePos, Vector3.down);
            RaycastHit hitPoint;
 
            if(Physics.Raycast(ray, out hitPoint))
            {
                if(hitPoint.collider.CompareTag("Terrain"))
                {
                    return hitPoint.point;
                }                
            }

            return Vector3.zero;
        }     
#endregion  

#region Fields
        [SerializeField] private UIManager m_uiManager = null;
        [SerializeField] private Camera m_camera = null;
        [SerializeField] private float m_edgeOffset = 10f;
        [SerializeField] private float m_speed = 500f; 
        private float m_zCamPos = 0;
        private PlayerController m_astronautSelected = null;
        private bool m_isDragging = false;
        private float m_clickTime = 0;
        private const float k_terrainLimitBoundary = 10000f;
        private const float k_clickToDraggTrheshold = 0.5f;
#endregion

    }
}