using System;
using UnityEngine;

namespace AlexSetoTest
{
    public class ItemController: MonoBehaviour
    {        
#region Public Methods
        public void Initialize(ItemModel model, Action<ItemModel> itemUsedCallback)
        {
            m_itemModel = model;
            m_itemUsedCallback = itemUsedCallback;
            m_itemView.Init(model);            
        }

        public void ApplyItem()
        {
            if(GameState.Instance.CurrentState == State.GameEnd)
            {
                return;
            }

            m_itemUsedCallback?.Invoke(m_itemModel);
            Destroy(gameObject);
        }
#endregion

#region Fields
        [SerializeField] private ItemView m_itemView = null;
        private ItemModel m_itemModel = null;
        private Action<ItemModel> m_itemUsedCallback = null;
#endregion
    }
}