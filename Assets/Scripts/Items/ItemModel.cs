using UnityEngine;

namespace AlexSetoTest
{
    public class ItemModel
    {
#region Properties
        public VitalType ItemVitalType => m_itemVitalType;
        public Sprite Icon => m_iconSprite;
        public int Amount => m_amount;
#endregion

#region Public Methods        
        public ItemModel(ItemConfig config)
        {
            m_itemVitalType = config.vitalType;
            m_iconSprite = config.m_vitalIcon;
            m_amount = config.vitalAmount;
        }
#endregion

#region Fields
        private VitalType m_itemVitalType;
        private Sprite m_iconSprite = null;
        private int m_amount = 0;
#endregion
    }
}
