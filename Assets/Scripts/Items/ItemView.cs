using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace AlexSetoTest
{
    public class ItemView : MonoBehaviour
    {        
#region Public Methods
        public void Init(ItemModel itemModel)
        {            
            m_icon.sprite = itemModel.Icon;
            m_itemText.text = itemModel.ItemVitalType.ToString();
        }
#endregion

#region Fields
        [SerializeField] private Image m_icon = null;
        [SerializeField] private TextMeshProUGUI m_itemText = null;
#endregion
    }
}
