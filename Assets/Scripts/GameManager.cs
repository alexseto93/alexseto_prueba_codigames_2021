using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AlexSetoTest
{
    public class GameManager : MonoBehaviour
    {        

#region Monobehaviour
        private void Start() 
        {
            StartGame();
        }
        
        void Update()
        {
            m_inputManager.CheckCameraScreenEdges();                        

            if(GameState.Instance.CurrentState == State.Playing)
            {      
                foreach (var character in m_charactersList)
                {
                    character.SetDestination();
                    character.GetClosestCrateInRange(m_cratesList);                
                }  

                m_inputManager.CheckUserInput();
            }          
        }        
#endregion

#region Public Methods
        public void ResetGame()
        {
            //TODO: Don't reload the scene, cleanup and start game again
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }

#endregion

#region Private Methods
        void StartGame()
        {          
            GameState.Instance.SetMatchState(State.Playing);
            OnPlayerDie += GameEnd;

            CreateAstronaut();
            InvokeRepeating("CreateCrate", 0f, m_gameConfig.spawnTime);            
        }

        private void CreateAstronaut()
        {
            if(GameState.Instance.CurrentState == State.GameEnd)
            {
                return;
            }

            Vector3 instantiatePos = m_inputManager.GetCenterPositionCamera();

            if(instantiatePos == Vector3.zero)
            {
                Debug.LogError("Trying to spawn an astronaut to an invalid position");
            }
            else
            {
                PlayerController character = Instantiate(m_astronautPrefab, instantiatePos, Quaternion.identity);
                character.InitCharacter(m_gameConfig.astronautConfig, m_waypoints, OnPlayerDie);
                m_charactersList.Add(character);
                character.SetAstronautName(k_defaultName + m_charactersList.Count.ToString());
                m_vitalSensorsPanel.InitNewCharacterAmount(character);                
            }            
        }

        private void CreateCrate()
        {           
            float randomX = UnityEngine.Random.Range(m_gameConfig.minXAreaSpawn, m_gameConfig.maxXAreaSpawn);
            float randomZ = UnityEngine.Random.Range(m_gameConfig.minZAreaSpawn, m_gameConfig.maxZAreaSpawn);
            Vector3 randomPos = new Vector3(randomX, k_yRayPos, randomZ);
            Vector3 cratePos = m_inputManager.GetYTerrainPositionAt(randomPos);            

            if(cratePos == Vector3.zero)
            {
                Debug.LogError("Trying to spawn a crate to an invalid position");
            }
            else
            {
                CrateController crate = Instantiate(m_cratePrefab, cratePos, Quaternion.identity);
                crate.InitCrate(m_gameConfig.itemConfigs, m_gameConfig.maxAmountItems, RemoveCrate);
                m_cratesList.Add(crate);
            }
        }

        private void RemoveCrate(CrateController crate)
        {
            m_cratesList.Remove(crate);            
        }

        private void GameEnd(PlayerController astronautDead)
        {
            GameState.Instance.SetMatchState(State.GameEnd);
            m_uiManager.OpenGameEndPanel(astronautDead);
            CancelInvoke();

            foreach (var character in m_charactersList)
            {
                character.OnGameEnds();                                
            }

            OnPlayerDie -= GameEnd;
        }        
#endregion

#region Fields
        private Action<PlayerController> OnPlayerDie;
        [SerializeField] private GameConfig m_gameConfig = null;
        [SerializeField] private UIManager m_uiManager = null;
        [SerializeField] private InputManager m_inputManager = null;
        [SerializeField] private VitalSensorsPanelController m_vitalSensorsPanel = null;
        [SerializeField] private PlayerController m_astronautPrefab = null;        
        [SerializeField] private CrateController m_cratePrefab;
        [SerializeField] private Transform[] m_waypoints = null;
        private List<PlayerController> m_charactersList = new List<PlayerController>();
        private List<CrateController> m_cratesList = new List<CrateController>();        
        private const string k_defaultName = "ASTRONAUT #";
        private const float k_yRayPos = 2000f;
#endregion
    }
}
