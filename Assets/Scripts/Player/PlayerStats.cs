namespace AlexSetoTest
{  
    public class PlayerStats
    {

#region Properties
        public Vitals[] CharVitals => m_charVitals;
#endregion

#region Constructor
        public PlayerStats(Vitals[] charVitals)
        {
            m_charVitals = charVitals;      
            m_maxFood = charVitals[(int)VitalType.Food].maxAmount;
            m_maxWater = charVitals[(int)VitalType.Water].maxAmount;
            m_maxOxygen = charVitals[(int)VitalType.Oxygen].maxAmount;

            m_currentFood = m_maxFood;
            m_currentWater = m_maxWater;
            m_currentOxygen = m_maxOxygen;
        }
#endregion

#region Public Methods
        public void IncreaseStat(ItemModel item)
        {
            switch(item.ItemVitalType)
            {
                case VitalType.Food:
                    m_currentFood += item.Amount;
                    m_currentFood = m_currentFood > m_maxFood ? m_maxFood : m_currentFood;
                    break;
                case VitalType.Water:
                    m_currentWater += item.Amount;
                    m_currentWater = m_currentWater > m_maxWater ? m_maxWater : m_currentWater;
                    break;
                case VitalType.Oxygen:
                    m_currentOxygen += item.Amount;
                    m_currentOxygen = m_currentOxygen > m_maxOxygen ? m_maxOxygen : m_currentOxygen;
                    break;
            }
        }

        public void DecreaseStat(Vitals vital)
        {
            switch(vital.vitalType)
            {
                case VitalType.Food:
                    m_currentFood -= vital.decreaseAmount;                    
                    break;
                case VitalType.Water:
                    m_currentWater -= vital.decreaseAmount;
                    break;
                case VitalType.Oxygen:
                    m_currentOxygen -= vital.decreaseAmount;
                    break;
            }

            IsAstronautAlive();
        }

        public float GetCurrentStatAmountNormalized(VitalType vitalType)
        {
            float amountNormalized = 0;
            switch(vitalType)
            {
                case VitalType.Food:
                    amountNormalized = m_currentFood/m_maxFood;
                    break;
                case VitalType.Water:
                    amountNormalized = m_currentWater/m_maxWater;
                    break;
                case VitalType.Oxygen:
                    amountNormalized = m_currentOxygen/m_maxOxygen;
                    break;
            }

            return amountNormalized;
        }

        public bool IsAstronautAlive()
        {
            if(m_currentFood <= 0 || m_currentWater <= 0 || m_currentOxygen <= 0)
            {
                return false;
            }

            return true;
        }

#endregion

#region Fields
        private Vitals[] m_charVitals = null;
        private float m_currentFood, m_currentWater, m_currentOxygen;
        private float m_maxFood, m_maxWater, m_maxOxygen;
#endregion
    }
}
