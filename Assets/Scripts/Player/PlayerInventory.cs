using System.Collections.Generic;

namespace AlexSetoTest
{
    public class PlayerInventory
    {
#region Properties
        public List<ItemModel> ItemsList => m_itemsList;   
#endregion

#region Public Methods            

        public void AddItems(List<ItemModel> itemsList)
        {
            m_itemsList.AddRange(itemsList);
        }

        public void RemoveItem(ItemModel item)
        {
            m_itemsList.Remove(item);
        }
#endregion

#region Fields
        private List<ItemModel> m_itemsList = new List<ItemModel>();
#endregion
    }
}