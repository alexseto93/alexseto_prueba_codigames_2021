using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AlexSetoTest
{
    [RequireComponent(typeof(NavMeshAgent), typeof(Rigidbody))]
    public class PlayerController : MonoBehaviour
    {

#region Events
        public Action OnOpenedCrate;
        public Action<VitalType> ItemUsed;
        public Action<VitalType> StatDecreased;
#endregion

#region Properties        
        public PlayerStats CharacterStats => m_playerStats;
        public PlayerInventory CharacterInventory => m_charInventory;
        public string AstronautName => m_astronautName; 
#endregion

#region Public Methods

        public void InitCharacter(AstronautConfig charConfig, Transform[] waypoints, Action<PlayerController> onPlayerDie)
        {
            m_playerStats = new PlayerStats(charConfig.vitals);
            m_charInventory = new PlayerInventory();
            m_visionRange = charConfig.visionRange;
            m_waypoints = waypoints;
            m_pointIndex = UnityEngine.Random.Range(0, waypoints.Length);
            m_playerDied = onPlayerDie;             

            foreach (Vitals stat in charConfig.vitals)
            {
                StartCoroutine(DecreaseStats(stat));
            }
        }     

        public void SetAstronautName(string name)
        {
            m_astronautName = name;
        }

        public void SetDestination()
        {    
            if (m_navMeshAgent.velocity.magnitude > 0f)
            {
                m_animator.SetFloat("Speed", m_navMeshAgent.velocity.magnitude / m_navMeshAgent.speed);
            }

            if(m_nearestCrate)
            {   
                if(!m_nearestCrate.IsOpened)
                {
                    m_navMeshAgent.SetDestination(m_nearestCrate.transform.position); 
                }
                else
                {
                    m_nearestCrate = null;
                    m_navMeshAgent.ResetPath();
                }
            }
            else if (!m_navMeshAgent.pathPending && m_navMeshAgent.remainingDistance < k_minDistanceDestination)
            {
                m_navMeshAgent.SetDestination(m_waypoints[m_pointIndex].position);
                m_pointIndex = UnityEngine.Random.Range(0, m_waypoints.Length);
            }         
        }

        public void GetClosestCrateInRange(List<CrateController> crates)
        {     
            foreach(CrateController crate in crates)
            {
                Vector3 distance = crate.transform.position - transform.position;
                float distSqr = distance.sqrMagnitude;
                if(distSqr < m_nearestDistance && distSqr <= m_visionRange * m_visionRange)
                {
                    m_nearestDistance = distSqr;
                    m_nearestCrate = crate;                    
                }
            }
        }

        public void OnItemUsed(ItemModel item)
        {
            m_playerStats.IncreaseStat(item);
            m_charInventory.RemoveItem(item);
            ItemUsed?.Invoke(item.ItemVitalType);
        } 

        public void OnGameEnds()
        {
            StopAllCoroutines();
            m_navMeshAgent.isStopped = true;
            m_navMeshAgent.speed = 0f;
            m_animator.SetFloat("Speed", 0f);
        }

        public void OnBeginDrag()
        {
            m_navMeshAgent.isStopped = true;
            m_animator.SetBool("Drag", true);
        }

        public void OnEndDrag()
        {
            m_navMeshAgent.isStopped = false;
            m_animator.SetBool("Drag", false);
        }   
#endregion

#region Private Methods

        private IEnumerator DecreaseStats(Vitals vital)
        {          
            while (m_playerStats.IsAstronautAlive())
            {                   
                yield return new WaitForSeconds(vital.decreaseTime);
                m_playerStats.DecreaseStat(vital);
                StatDecreased?.Invoke(vital.vitalType);                
            }

            Die();
            m_playerDied?.Invoke(this);            
        }

        private void Die()
        {
            //TODO: Die animation            
            Debug.Log("Player die");            
        }

        void OnTriggerEnter(Collider collider)
        {
            if(collider.gameObject.CompareTag("Crate") && GameState.Instance.CurrentState != State.GameEnd)
            {
                collider.gameObject.GetComponent<CrateController>().OpenCrate(this);
                OnOpenedCrate?.Invoke();        
                m_nearestDistance = float.MaxValue;        
            }
        }   

        private void OnDrawGizmos() 
        {
            if(m_showDebugRange)
            {
                Gizmos.DrawWireSphere(transform.position, m_visionRange);
            }        
        }
#endregion

#region Fields
        private Action<PlayerController> m_playerDied = null;
        [SerializeField] private NavMeshAgent m_navMeshAgent = null;
        [SerializeField] private Animator m_animator = null;
        [SerializeField] private bool m_showDebugRange = false;
        private string m_astronautName = string.Empty;
        private PlayerStats m_playerStats = null;
        private PlayerInventory m_charInventory = null;
        private Transform[] m_waypoints = null;
        private int m_pointIndex = 0;
        private float m_visionRange = 0;
        private CrateController m_nearestCrate = null;       
        private float m_nearestDistance = float.MaxValue;
        private const float k_minDistanceDestination = 0.5f;
        
#endregion
    }
}
