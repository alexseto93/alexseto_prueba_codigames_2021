namespace AlexSetoTest
{
    [System.Serializable]
    public enum VitalType
    {
        Food,
        Water,
        Oxygen
    }    

    [System.Serializable]
    public class Vitals
    {
        public VitalType vitalType;
        public int maxAmount;
        public int decreaseTime;
        public int decreaseAmount;
    }
}