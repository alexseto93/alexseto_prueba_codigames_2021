using System;
using System.Collections.Generic;
using UnityEngine;

namespace AlexSetoTest
{
    public class CrateController : MonoBehaviour
    {
#region Properties
        public bool IsOpened => m_isOpened;
#endregion

#region Public Methods

        public void InitCrate(ItemConfig[] itemsConfig, int maxAmountItems, Action<CrateController> removeCrateCallback)
        {
            m_removeCrateCallback = removeCrateCallback;
            SetRandomItems(maxAmountItems, itemsConfig);
        }        

        public void OpenCrate(PlayerController character)
        {
            m_removeCrateCallback?.Invoke(this);
            m_isOpened = true;
            m_animation.Play();
            GetComponent<Collider>().enabled = false;
            character.CharacterInventory.AddItems(m_item);
        }
#endregion

#region Private Methods
        private void SetRandomItems(int maxAmount, ItemConfig[] itemsConfig)
        {
            m_amountItems = UnityEngine.Random.Range(1, maxAmount);
            int randomType = 0;

            for (int i = 0; i < m_amountItems; i++)
            {
                randomType = UnityEngine.Random.Range(0, itemsConfig.Length);
                m_item.Add(new ItemModel(itemsConfig[randomType]));
            }            
        }
#endregion

    #region Fields
        [SerializeField] private Animation m_animation;
        private Action<CrateController> m_removeCrateCallback = null;
        private List<ItemModel> m_item = new List<ItemModel>();
        private bool m_isOpened = false;
        private int m_amountItems = 0;
    #endregion
    }
}
