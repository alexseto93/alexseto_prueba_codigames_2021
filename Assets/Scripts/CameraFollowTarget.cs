using UnityEngine;

namespace AlexSetoTest
{    public class CameraFollowTarget : MonoBehaviour
    {
#region Monobehaviour
        void Update()
        {
            if(m_target)
            {
                Vector3 forwardOffPos = m_target.position + (m_target.forward * m_distanceOffset);
                Vector3 camOffPos = new Vector3(forwardOffPos.x + m_camXOffset, forwardOffPos.y + m_camYOffset, forwardOffPos.z);
                Vector3 astronautPos = new Vector3(m_target.position.x, m_target.position.y + m_astronautYPivotOffset, m_target.position.z);

                transform.position = camOffPos;
                transform.LookAt(astronautPos);
            }            
        }
#endregion

#region Public Methods
        public void SetTargetToFollow(Transform target)
        {
            m_target = target;
        }
#endregion 

#region Fields

        [SerializeField] private float m_distanceOffset = 75f;
        [SerializeField] private float m_camXOffset = 30f;        
        [SerializeField] private float m_camYOffset = 50f;
        [SerializeField] private float m_astronautYPivotOffset = 40f;
        private Transform m_target = null;     
#endregion    
    }
}
