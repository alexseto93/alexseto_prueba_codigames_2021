namespace AlexSetoTest
{
    public enum State
    {
        Playing,
        GameEnd
    }

    public class GameState 
    {
#region Properties        
        public State CurrentState
        {
            get { return m_currentState; }
        }
#endregion

#region Constructor

        private GameState()
        {
            m_currentState = State.Playing;
        }
#endregion

#region Public Static Methods

        public static GameState Instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = new GameState();
                }

                return m_instance;
            }
        }
#endregion

#region Public Methods

        public void SetMatchState(State newState)
        {
            m_currentState = newState;
        }
#endregion

#region Fields
        private static GameState m_instance = null;
        private State m_currentState;
#endregion
    }
}
